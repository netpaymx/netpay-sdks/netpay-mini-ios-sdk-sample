//
//  ViewController.swift
//  FrameworkTest
//
//  Created by Andres Avila on 12/27/19.
//  Copyright © 2019 Andres Avila. All rights reserved.
//

import UIKit
import NetPayMiniSDK
import CoreLocation

class ViewController: UIViewController {
    
    @IBOutlet weak var transactionIdLbl: UILabel!
    
    var transactionId: String? {
        didSet {
            transactionIdLbl.text = transactionId
        }
    }
    
    lazy var service: NPService! = {
        return NPService()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }

    @IBAction func transaction() {
        
        service.transaction(presentingVC: self,
                            amount: 1.0,
                            deviceName: "MPOS8083100219",
                            location: CLLocationCoordinate2D(latitude: 25.6495641, longitude: -100.3255365),
                            promotion: .noPromotion,
                            tip: 0.0,
                            reference: "referencia",
                            enableSignature: true,
                            delegate: self)
        
    }
    
    @IBAction func cancel() {

        service.cancelTransaction()
        
    }
    
    @IBAction func refund() {
        
        guard let transactionId = transactionId else { return }
        
        service.refund(transactionId: transactionId) { success, errorMessage in
            
            if success {
                
                print("Se canceló la venta")
                
            } else {
                
                print(errorMessage ?? "No se recibió error")
                
            }
            
        }
        
    }

}

extension ViewController: TransactionDelegate {
    
    func didBeginConnectingDevice() {
        print("didBeginConnectingDevice")
    }
    
    func connectingBluetooth() {
        print("connectingBluetooth")
    }
    
    func bluetoothNotFound() {
        print("bluetoothNotFound")
    }
    
    func bluetoothIsOff() {
        print("bluetoothIsOff")
    }
    
    func insertSwipeApproachCard() {
        print("insertSwipeApproachCard")
    }
    
    func readingCardChip() {
        print("readingCardChip")
    }
    
    func deviceError(error: DeviceError) {
        print("deviceError: ", error)
    }
    
    func transactionError(message: String) {
        print("TransactionError: ", message)
    }
    
    func sendingTransaction() {
        print("sendingTransaction")
    }
    
    func didCompleteTransaction(transactionId: String) {
        print("didCompleteTransaction: ", transactionId)
        
        self.transactionId = transactionId
    }
    
}
